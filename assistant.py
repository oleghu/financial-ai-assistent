from neuralintents import GenericAssistant
import matplotlib.pyplot as plt
import pandas as pd
import pandas_datareader as web
import mplfinance as mpf

import pickle
import sys
import datetime as dt


with open('portfolio.pkl', 'rb') as f:
    portfolio = pickle.load(f)

def save_portifolio():
    with open('portfolio.pkl', 'wb') as f:
        pickle.dump(portfolio, f)

def add_portfolio():
    ticker = input("Hvilken aksje vil du legge til: ")
    amount = input("Hvor mange akjser vil du legge til: ")

    if ticker in portfolio.keys():
        portfolio[ticker] += int(amount)
    else:
        portfolio[ticker] = int(amount)

    save_portifolio()

def remove_portfolio():
    ticker = input("Hvilken aksje ønsker du å selge: ")
    amount = input("Hvor mange akjser ønsker du å selge: ")

    if ticker in portfolio.keys():
        if int(amount) <= portfolio[ticker]:
            portfolio[ticker] -= int(amount)
            save_portifolio()
        else:
            print("Du eier ikke så mange akjser i dette selskapet")
    else:
        print(f"Du eier ingen akjser i {ticker}")

def show_portfolio():
    print("Din protefølje: ")
    for ticker in portfolio.keys():
        print(f"Du eier {portfolio[ticker]} aksjer i {ticker}")

def portfolio_worth():
    sum = 0
    for ticker in portfolio.keys():
        data = web.DataReader(ticker, 'yahoo')
        price = data['Close'].iloc[-1]
        sum += int(price)
    print(f"Din protefølje er verdt {sum} USD")

def portfolio_gain():
    starting_date = input("Legg til dato for sammenligning (YYYY-MM-DD): ")

    sum_now = 0
    sum_then = 0

    try:
        for ticker in portfolio.keys():
            data = web.DataReader(ticker, 'yahoo')
            price_now = data['Close'].iloc[-1]
            price_then = data.loc[data.index == starting_date]['Close'].values[0]
            sum_now += int(price_now)
            sum_then += int(price_then)

        print(f"Relativ profitt: {(sum_now-sum_then)/sum_then * 100}%")
        print(f"Absolutt profitt: {sum_now-sum_then} USD")
    except IndexError:
        print("Det var ingen handel denne dagen!")

def plot_chart():
    ticker = input("Velg en aksje: ")
    starting_string = input("Velg en start dato (DD/MM/YYYY): ")

    plt.style.use('dark_background')

    start = dt.datetime.strptime(starting_string, "%d/%m/%Y")
    end = dt.datetime.now()

    data = web.DataReader(ticker, 'yahoo', start, end)

    colors = mpf.make_marketcolors(up='#00ff00', down='#ff0000', wick='inherit', edge='inherit', volume='in')

    mpf_style = mpf.make_mpf_style(base_mpf_style='nightclouds', marketcolors=colors)
    mpf.plot(data, type='candle', style=mpf_style, volume=True)

def bye():
    print("Ha det bra, sjef")
    sys.exit(0)

def myfunction():
    return

mappings = {
    'plot_chart': plot_chart,
    'add_portfolio': add_portfolio,
    'remove_portfolio': remove_portfolio,
    'show_portfolio': show_portfolio,
    'portfolio_worth': portfolio_worth,
    'portfolio_gains': portfolio_gain,
    'bye': bye
}

assistant = GenericAssistant('intents.json', intent_methods=mappings)

#assistant.train_model()

assistant.load_model()

while True:
    message = input("")
    assistant.request(message)